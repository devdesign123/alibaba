import { Component } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { RequestQuotationComponent } from './request-quotation/request-quotation.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(
    private router: Router
    ){}

  // For Headers Change
  showNewHeader(){
    if(this.router.url==='/emailVerify' || this.router.url==='/register' || this.router.url==='/registerComplete') {
      return true;
    } else {
      return false;
    }
  }
  
}
