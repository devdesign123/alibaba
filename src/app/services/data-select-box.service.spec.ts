import { TestBed, inject } from '@angular/core/testing';

import { DataSelectBoxService } from './data-select-box.service';

describe('DataSelectBoxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataSelectBoxService]
    });
  });

  it('should be created', inject([DataSelectBoxService], (service: DataSelectBoxService) => {
    expect(service).toBeTruthy();
  }));
});
