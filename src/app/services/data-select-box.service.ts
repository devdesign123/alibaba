import { Injectable } from '@angular/core';
import { MainCategory } from '../request-quotation/main-category';
import { SubCategoryOne } from '../request-quotation/sub-category.1';
import { SubCategoryTwo } from '../request-quotation/sub-category.2';
import { SubCategoryThree } from '../request-quotation/sub-category.3';

@Injectable()
export class DataSelectBoxService {
  
  getMainCategory(){
    return[
      new MainCategory(1, 'Agriculture'),
      new MainCategory(2, 'Apparel' ),
      new MainCategory(3, 'Automobiles & Motorcycles' ),
      new MainCategory(4, 'Beauty & Personal Care' ),
      new MainCategory(5, 'Business Services' ),
      new MainCategory(6, 'Chemicals' )
    ]
  }

  getSubCategoryOne(){
    return[
      new SubCategoryOne(1, 1, 'Former'),
      new SubCategoryOne(2, 1, 'Garden'),
      new SubCategoryOne(3, 2, 'Pants'),
      new SubCategoryOne(4, 2, 'Shirts'),
      new SubCategoryOne(5, 3, 'Ford'),
      new SubCategoryOne(6, 3, 'Suzuki'),
      new SubCategoryOne(7, 4, 'Trimmer'),
      new SubCategoryOne(8, 4, 'Gillet'),
      new SubCategoryOne(9, 5, 'GST'),
      new SubCategoryOne(10, 6, 'Organic')
    ]
  }

  getSubCategoryTwo(){
    return[
      new SubCategoryTwo(1, 1, 'Rural' ),
      new SubCategoryTwo(2, 1, 'Urban' ),
      new SubCategoryTwo(3, 2, 'Flower'),
      new SubCategoryTwo(4, 2, 'Plants'),
      new SubCategoryTwo(5, 3, 'Full sleave' ),
      new SubCategoryTwo(6, 3, 'Half sleave'),
      new SubCategoryTwo(7, 4, 'Two seater' ),
      new SubCategoryTwo(8, 4, 'Four Seater' ),
    ]
  }

  getSubCategoryThree(){
    return[
      new SubCategoryThree(1, 1, 'Lower rural' ),
      new SubCategoryThree(2, 2, 'Upper urban' ),
      new SubCategoryThree(3, 3, 'Rose'),
      new SubCategoryThree(4, 4, 'Paddy'),
      new SubCategoryThree(5, 5, 'Cotton' ),
      new SubCategoryThree(6, 6, 'Polyster'),
      new SubCategoryThree(7, 7, 'Luxery' ),
      new SubCategoryThree(8, 8, 'Sports' ),
    ]
  }
  constructor() { }

}
