import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderRequestQuotationComponent } from './header-request-quotation.component';

describe('HeaderRequestQuotationComponent', () => {
  let component: HeaderRequestQuotationComponent;
  let fixture: ComponentFixture<HeaderRequestQuotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderRequestQuotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderRequestQuotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
