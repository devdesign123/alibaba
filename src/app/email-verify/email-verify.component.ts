import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-email-verify',
  templateUrl: './email-verify.component.html',
  styleUrls: ['./email-verify.component.css']
})
export class EmailVerifyComponent implements OnInit {

  constructor(private router: Router) { }

  SubmitEmailVerify = function(){
    this.router.navigateByUrl('/register');
  }

  ngOnInit() {
  }

}
