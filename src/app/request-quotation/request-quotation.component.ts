import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DataSelectBoxService } from '../services/data-select-box.service';

import { MainCategory } from './main-category';
import { SubCategoryOne } from './sub-category.1';
import { SubCategoryTwo } from './sub-category.2';
import { SubCategoryThree } from './sub-category.3';

@Component({
  selector: 'app-request-quotation',
  templateUrl: './request-quotation.component.html',
  providers: [DataSelectBoxService],
  styleUrls: ['./request-quotation.component.css']
})
export class RequestQuotationComponent implements OnInit {
  selectedItem: any = '';
  inputChanged: any = '';
  UrgentRequestState: boolean;

  private quotationForm:FormGroup;

  constructor(
    private _DataSelectBoxService: DataSelectBoxService, 
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.MainCategories = this._DataSelectBoxService.getMainCategory();
    this.UrgentRequestState = false;

    
    this.quotationForm = this.formBuilder.group({
      prodCat: ['', Validators],
      MainCategory: [''],
      SubCategoryOne: [''],
      SubCategoryTwo: [''],
      SubCategoryThree: [''],
      quantity: ['', Validators.required],
      image: [''],
      UnitPrice: [''],
      DestPort: [''],
      BusinessCard: [''],
      TmsRfq: ['', Validators.required]
    });    
  };

  SubmitQuotaion = function(){
    let prodCat = this.selectedItem.payload.label;
    this.quotationForm.value.prodCat = prodCat;

    //console.log(this.quotationForm.value);
    
    this.router.navigateByUrl('/emailVerify');
  }

  // Auto Complete Data Start
  items2: any[] = [{id: 0, payload: {label: 'Tom'}},
    {id: 1, payload: {label: 'John'}},
    {id: 2, payload: {label: 'Lisa'}},
    {id: 3, payload: {label: 'Js'}},
    {id: 4, payload: {label: 'Java'}},
    {id: 5, payload: {label: 'c'}},
    {id: 6, payload: {label: 'vc'}}
  ];
  config2: any = {'placeholder': 'Key words of products you are looking for', 'sourceField': ['payload', 'label']};

  onSelect(item: any) {
    this.selectedItem = item;
  }
 
  onInputChangedEvent(val: string) {
    this.inputChanged = val;
  }
  // Auto Complete Data End

  // Select Category Data Start
  selectedMainCategory:MainCategory = new MainCategory(0, 'India'); 
  MainCategories: MainCategory[];
  SubCategoriesOne: SubCategoryOne[];
  SubCategoriesTwo: SubCategoryTwo[];
  SubCategoriesThree: SubCategoryThree[];
  
  onSelectMainCategory(MainCategoryid) {
    this.SubCategoriesOne = this._DataSelectBoxService.getSubCategoryOne().filter((item)=> item.MainCategoryid == MainCategoryid);
  }
  onSelectSubCategoryOne(SubCategoryOneid) {
    this.SubCategoriesTwo = this._DataSelectBoxService.getSubCategoryTwo().filter((item)=> item.SubCategoryOneid == SubCategoryOneid);
  }
  onSelectSubCategoryTwo(SubCategoryTwoid) {
    this.SubCategoriesThree = this._DataSelectBoxService.getSubCategoryThree().filter((item)=> item.SubCategoryTwoid == SubCategoryTwoid);
  }

  
  // Select Category Data End

  // Quantity Pieces Select Option Start
  QuantitySelect: any[] = [
    {id: 0, Name: "20' Container"},
    {id: 1, Name: "40' Container"},
    {id: 2, Name: "40' HQ Container"},
    {id: 3, Name: "Acre/Acres"},
    {id: 4, Name: "Ampere/Amperes"},
    {id: 5, Name: "Bags"},
    {id: 6, Name: "Barrel/Barrels"},
    {id: 7, Name: "Boxes"},
    {id: 8, Name: "Bushel/Bushels"},
    {id: 9, Name: "Carat/Carats"},
    {id: 10, Name: "Carton/Cartons"},
    {id: 11, Name: "Case/Cases"},
    {id: 12, Name: "Centimeter/Centimeters"},
    {id: 13, Name: "Chain/Chains"}
  ];
  // Quantity Pieces Select Option End

  // Other Requarement Select Option End
  OtherSelectFOB: any[] = [
    {id: 0, Name: "FOB"},
    {id: 1, Name: "EXW"},
    {id: 2, Name: "FAS"},
    {id: 3, Name: "FCA"},
    {id: 4, Name: "CFR"},
    {id: 5, Name: "CPT"},
    {id: 6, Name: "CIF"},
    {id: 7, Name: "CIP"},
    {id: 8, Name: "DES"},
    {id: 9, Name: "DAF"},
    {id: 10, Name: "DEQ"},
    {id: 11, Name: "DDP"},
    {id: 12, Name: "DDU"}
  ];

  PriceSelectType: any[] = [
    {id: 0, Name: "USD"},
    {id: 1, Name: "GBP"},
    {id: 2, Name: "RMB"},
    {id: 3, Name: "EUR"},
    {id: 4, Name: "AUD"},
    {id: 5, Name: "CAD"},
    {id: 6, Name: "CHF"},
    {id: 7, Name: "JPY"},
    {id: 8, Name: "HKD"},
    {id: 9, Name: "NZD"},
    {id: 10, Name: "SGD"},
    {id: 11, Name: "NTD"},
    {id: 12, Name: "BRL"}
  ];

  MoneyTransferSelect: any[] = [
    {id: 0, Name: "T/T"},
    {id: 1, Name: "L/C"},
    {id: 2, Name: "D/P"},
    {id: 3, Name: "Western Union"},
    {id: 4, Name: "Money Gram"},
  ];

  // Other Requarement Select Option End


  //Urgent Request Box Start
  TotalServiceFree = "0.00"
  UrgentRequestRate = '9.99'
  ExtraQuotesRate = '2.99'
  UrgentRequestFun = function(){
    this.UrgentRequestState = !this.UrgentRequestState;
    if(this.UrgentRequestState == true){
      this.TotalServiceFree = Number(this.TotalServiceFree) + Number(this.UrgentRequestRate);
    }
    else{
      this.TotalServiceFree = Number(this.TotalServiceFree) - Number(this.UrgentRequestRate);
    }
  }
  ExtraQuotesFun = function(){
    this.ExtraQuotesState = !this.ExtraQuotesState;
    if(this.ExtraQuotesState == true){
      this.TotalServiceFree = Number(this.TotalServiceFree) + Number(this.ExtraQuotesRate);
    }
    else{
      this.TotalServiceFree = Number(this.TotalServiceFree) - Number(this.ExtraQuotesRate);
    }
  }

  TotalSFree = function(){
    this.TotalServiceFree = this.UrgentRequestRate + this.ExtraQuotesRate;
  }
  


  ngOnInit() {
  }
}