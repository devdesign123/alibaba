import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { RequestQuotationComponent } from './request-quotation/request-quotation.component';
import { EmailVerifyComponent } from './email-verify/email-verify.component';
import { RegistrationComponent } from './registration/registration.component';
import { RegisterCompleteComponent } from './register-complete/register-complete.component';

export const router: Routes = [
    {path:'', pathMatch:'full', component:RequestQuotationComponent},
    {path:'requestQuotation', component:RequestQuotationComponent},
    {path:'emailVerify', component:EmailVerifyComponent},
    {path:'register', component:RegistrationComponent},
    {path:'registerComplete', component:RegisterCompleteComponent},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routers: ModuleWithProviders = RouterModule.forRoot(router);