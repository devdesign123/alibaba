import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RequestQuotationComponent } from './request-quotation/request-quotation.component';
import { routers } from './app.routes';
import { FooterComponent } from './footer/footer.component';
import { HeaderRequestQuotationComponent } from './header-request-quotation/header-request-quotation.component';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import { EmailVerifyComponent } from './email-verify/email-verify.component';
import { RegistrationComponent } from './registration/registration.component';
import { RegisterCompleteComponent } from './register-complete/register-complete.component';
import { HeaderRegistrationComponent } from './header-registration/header-registration.component';
import { NumberInputDirective } from './directives/number-input.directive';

@NgModule({
  declarations: [
    AppComponent,
    RequestQuotationComponent,
    FooterComponent,
    HeaderRequestQuotationComponent,
    EmailVerifyComponent,
    RegistrationComponent,
    RegisterCompleteComponent,
    HeaderRegistrationComponent,
    NumberInputDirective
  ],
  imports: [
    BrowserModule,
    routers,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
